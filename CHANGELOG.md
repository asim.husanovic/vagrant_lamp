# Dnevnik promjena
Sve značajne promjene za ovaj projekat će biti dokumentovane ovdje.

Format dokumenta je baziran na [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
i ovaj projekat će se pridržavati [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2019-10-13
### Dodano
- CHANGELOG.md datoteka
- README.md datoteka

### Promijenjeno
-

### Uklonjeno
- Section about "changelog" vs "CHANGELOG".

[Unreleased]: https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...HEAD
[1.0.0]: https://github.com/olivierlacan/keep-a-changelog/releases/tag/v1.0.0
