# Vagrant LAMP (Linux, Apache, MySQL, PHP)

VagrantLAMP je okvir koji se može iskoristiti za većinu web baziranih projekata koji ne zahtijevaju neke specijalne alate a koji su razvijani u PHP s MySQL/MariaDB bazom podataka

## Instalacija

U osnovi ovaj okvir se ne istalira neko se koristi za pokretanje dvostrukog virtualnog okruženja. Jedno je unutar VagrantBOX, a drugo podijeljeno unutar te kutije na nekoliko kontejnra. Svaki od gore posmenutih servisa trči u posebnom Docker kontejneru.

Za preuzimanje ovog okriva:
```
git clone git@gitlab.com:asim.husanovic/vagrant_lamp.git
```

## Upotreba

- Jednom preuzet okriv na mašini se pokreće jednostavno uz pomoć [vagrant](https://www.vagrantup.com/downloads.html) naredbi:
```
cd <putana_do_vagrant_lamp_okvira>
vagrant up --provision
```

- Da bi se VagrantLAMP box zaustavio neophodno je koristiti naredbu:
```
cd <putana_do_vagrant_lamp_okvira>
vagrant down
```

- Ako se želi iz bilo kog razloga izbisati virtualno okruženje napravljeno gornjom naredbom koristi se:
```
cd <putana_do_vagrant_lamp_okvira>
vagrant destroy -f
```

- Za pristup virutalnom okruženju VirtualBox-a koristi se naredba:
```
cd <putana_do_vagrant_lamp_okvira>
vagrant ssh
```

- Za prikaz pokrenutih Docker kontejnera koristi se naredba unutar virtualnog okružena VirtualBox:
```
docker ps
```
Ispis bi trebao da bude nalik:
```
CONTAINER ID        IMAGE                           COMMAND                  CREATED              STATUS              PORTS                                      NAMES
277055fe55f4        miveo/centos-httpd-for-phpfpm   "/run-httpd.sh"          About a minute ago   Up About a minute   0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   ah-webserver
cec2b197effc        mysql:8.0                       "docker-entrypoint.s…"   About a minute ago   Up About a minute   0.0.0.0:3306->3306/tcp, 33060/tcp          ah-mysql
30bf8fffea3e        miveo/centos-php-fpm:7.2        "php-fpm -F"             About a minute ago   Up About a minute                                              ah-php-fpm
```

- Za pristup svakom od Docker kontejnera korsti se naredba unutar virtualnog okruženja VirtualBox:
```
docker exec -ti <ime_kontjenera> /bin/bash
```

- Nakon pokretanja virtualnog okruženja VirtualBox, automatski se pokreću servisi u Docker kontejnerima s već naprijed spomenutim parametrom `--provision`, te se ima pristup ka URL-u:
http://192.168.10.101

_Napomena:_
- Ovaj okvir sadržava konfiguraciju za automatsku preinstalaciju (engl. provisioning) svih neophodnih paketa prilikom kreiranja virtualnog okruženja. Da bi se ta naradba uključila zbog toga se koristi argument `--provision`
- Parametar `-f` kod brisanja odnosno totalnog uništenja virtualnog okruženja olakšava krajnjem korisniku brzinu postupka, ako zna šta radi. Naime s ovim parametrom `vagrant` naredba nas neće pitati da potvrdimo da li smo sigurni za nastavak.

## Pomoć za otklanjanje problema

- Ako se na sistemu već ima instaliran vagrant od prije i pojavljuju se neke neočekivane greške. Pokušajte s nadogradnjom
`vagrant plugin update`

- Ako se pojavi greška nemogućnosti mounta vboxsf diska, tada je vjerovatno neophodno pokrenuti naredbu:
`vagrant plugin install vagrant-vbguest`

## Doprinos

Svaki zahtjev za unaprjeđenjem je dobrodošao. Ako imate zahtjeve za velike promjene, prvo otvorite pitanje/diskusiju kako bi se svi uključili i porazgovarali o tome šta želite promijeniti.

Nemojte nikada zaboraviti ažurirati testove i pokrenuti ih prije nego pošaljete zahtjev za spajanje vašeg koda u jednu cjelinu.

## Licenca
[MIT](https://choosealicense.com/licenses/mit/)
