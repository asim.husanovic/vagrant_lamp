#!/bin/bash

echo "alias ll='ls -lGH'" >> /home/vagrant/.bashrc
echo "alias l='ls -lha'" >> /home/vagrant/.bashrc

echo "parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}" >> /home/vagrant/.bashrc

echo "export PS1='\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ '" >> /home/vagrant/.bashrc
